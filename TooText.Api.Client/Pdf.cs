﻿using System.IO;
using System.Net.Http;

namespace TooText.Api.Client
{
    public static class Pdf
    {
        //TODO: not an ideal place for this stuff
        private const string ENDPOINT = "https://tootextapimanagement.azure-api.net/pdf";
        private const string APIKEYNAME = "Ocp-Apim-Subscription-Key";
        private const string APIKEYVALUE = "3436370c4de9401093bf2afd93b0827d";

        public static string GetPdfText(MemoryStream ms)
        {
            using var client = new HttpClient();
            client.DefaultRequestHeaders.Add(APIKEYNAME, APIKEYVALUE);

            var bits = new ByteArrayContent(ms.ToArray());
            var response = client.PostAsync(ENDPOINT, bits);
            var result = response.Result.Content.ReadAsStringAsync().Result;

            return result;
        }
    }
}
