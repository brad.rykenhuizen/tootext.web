﻿using System.IO;
using Microsoft.AspNetCore.Http;

namespace TooText.Web.Business
{
    public static class Extensions
    {
        public static MemoryStream GetMemoryStream(this IFormFile file)
        {
            using var ms = new MemoryStream();
            file.CopyTo(ms);
            return ms;
        }
    }
}
