﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace TooText.Web.Business
{
    public static class FormValidation
    {

        private delegate string ValidationDelegate(IFormFile file);

        private static readonly Dictionary<string, string> SupportedFormatsDictionary = new Dictionary<string, string>()
        {
            {"PDF", "application/pdf"}
        };

        private static readonly List<ValidationDelegate> ValidationDelegates = new List<ValidationDelegate>()
        {
            IsThereAFile,
            IsFileSupported
        };

        private static string IsThereAFile(IFormFile file)
        {
            return file == null ? "No file selected." : "";
        }

        private static string IsFileSupported(IFormFile file)
        {
            var supportedFileText = $"{string.Join(",", SupportedFormatsDictionary.Keys)}";
            return SupportedFormatsDictionary.Keys.Contains(file?.ContentType ?? "") ? "" : $"File not supported. Supported Files: {supportedFileText}";
        }

        public static string ValidateForm(IFormFile file)
        {
            return ValidationDelegates.Aggregate("", (current, validation) => current + "<br />" + validation(file));
        }
    }
}
