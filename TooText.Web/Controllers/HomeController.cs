﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using TooText.Web.Models;
using Microsoft.AspNetCore.Http;
using TooText.Web.Business;

namespace TooText.Web.Controllers
{
    public class HomeController : Controller
    {

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.Result = "Select a PDF and click 'Get Text!' to get in on the action!";
            return View();
        }

        [HttpPost]
        public IActionResult Index(IFormFile file)
        {
            var errorText = FormValidation.ValidateForm(file);
            ViewBag.Result = errorText != "" ? errorText : Api.Client.Pdf.GetPdfText(file.GetMemoryStream());

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

    }
}
